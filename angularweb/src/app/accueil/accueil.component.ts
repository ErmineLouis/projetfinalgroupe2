import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  constructor(private route : Router) { }

  ngOnInit(): void {

  }

  btnInscription() {

    this.route.navigate(["/inscription"])

  }

  btnConnexion() {

    this.route.navigate(["/connexion"])
  }

}
