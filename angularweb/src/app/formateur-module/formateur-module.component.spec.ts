import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormateurModuleComponent } from './formateur-module.component';

describe('FormateurModuleComponent', () => {
  let component: FormateurModuleComponent;
  let fixture: ComponentFixture<FormateurModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormateurModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormateurModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
