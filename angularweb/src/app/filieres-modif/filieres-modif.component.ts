import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Filiere } from '../interfaces/filiere.interface';
import { FiliereService } from '../services/filiere.service';
import { FormationService } from '../services/formation.service';
import { Formation } from '../interfaces/formation.interface';
import { StagiaireService } from '../services/stagiaire.service';
import { Stagiaire } from '../interfaces/personne.interface';
@Component({
  selector: 'filieres-modif',
  templateUrl: './filieres-modif.component.html',
  styleUrls: ['./filieres-modif.component.css']
})
export class FilieresModifComponent implements OnInit {
  stagiaires:Stagiaire[]=[];
  libelle_formation:string ="";
  filiere: Filiere = {
    libelle: '',
    stagiaires: [],
  }
  formation: Formation ={
    libelle:""
  }
  filiere_libelle="";
  formations:Formation[]=[];
  actionText: 'Ajouter' | 'Modifier' = 'Ajouter';

  constructor(private filiereService: FiliereService,
     private http:HttpClient,
      private router: Router, 
      private route:ActivatedRoute,
      private formationService:FormationService,
      private stagiaireService: StagiaireService) { }

  ngOnInit(): void {
    this.formationService.getAll().subscribe((formations:Formation[])=>{this.formations=formations;
    const id: string | null = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.actionText = 'Modifier'
      this.filiereService.getById(Number(id))
        .subscribe((filiere: Filiere) => {
          this.filiere.libelle = this.libelle_formation;
        })
    }
  })
  }

  handleCustomEvent(event:any){
    const option = event.target.value;
    this.libelle_formation=option;
    console.log("libelle"+this.libelle_formation)
    this.stagiaireService.getByFomationLibelle(option)
    .subscribe((stagiaires:Stagiaire[])=>{this.stagiaires=stagiaires})
  }

  handleSubmit(){
    if (this.filiere.id) {
      console.log('Updating filière...');
      const { id, libelle } = this.filiere;
      const filiere: Filiere = { id, libelle, stagiaires: [] };
      this.filiereService.update(filiere)
        .subscribe(() => {
          this.router.navigate(['/gestionnaire-filieres']);
        })
    } else {
      console.log('Adding filière...');
      const { libelle } = this.filiere;
      const filiere: Filiere = {libelle, stagiaires: [] };
      const val= Math.round((Math.random()*100)+100);
      this.filiere.libelle=this.libelle_formation+'_'+val;
      
      this.filiereService.post(this.filiere)
        .subscribe(() => {
          this.router.navigate(['/gestionnaire-filieres']);
        })
    }
  }
















}
