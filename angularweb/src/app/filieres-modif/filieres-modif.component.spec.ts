import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilieresModifComponent } from './filieres-modif.component';

describe('FilieresModifComponent', () => {
  let component: FilieresModifComponent;
  let fixture: ComponentFixture<FilieresModifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilieresModifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilieresModifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
