import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormationsModifComponent } from './formations-modif.component';

describe('FormationsModifComponent', () => {
  let component: FormationsModifComponent;
  let fixture: ComponentFixture<FormationsModifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormationsModifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormationsModifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
