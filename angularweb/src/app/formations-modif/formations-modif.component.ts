import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormationService } from '../services/formation.service';
import { Formation } from '../interfaces/formation.interface';

@Component({
  selector: 'formations-modif',
  templateUrl: './formations-modif.component.html',
  styleUrls: ['./formations-modif.component.css']
})
export class FormationsModifComponent implements OnInit {
  formation:Formation={libelle:""};
  actionText: 'Ajouter' | 'Modifier' = 'Ajouter';
  constructor(private formationService: FormationService,
    private router:Router,
    private route:ActivatedRoute,
   ) { }

  ngOnInit(): void {
    const id: string | null = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.actionText = 'Modifier'
      this.formationService.getById(id)
        .subscribe((formation: Formation) => {
          this.formation = formation;
        })
    }
  }
  handleSubmit(){
    if (this.formation.id) {
      console.log('Mise à jour formaton...');
      const { id, libelle } = this.formation;
      const formation: Formation = { id, libelle };
      this.formationService.update(formation)
        .subscribe(() => {
          this.router.navigate(['/gestionnaire-formations']);
        })
    } else {
      console.log('Ajout de formation...');
      this.formationService.post(this.formation)
        .subscribe(() => {
          this.router.navigate(['/gestionnaire-formations']);
        })
    }
  }

}
