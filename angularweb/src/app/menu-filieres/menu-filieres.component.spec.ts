import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuFilieresComponent } from './menu-filieres.component';

describe('MenuFilieresComponent', () => {
  let component: MenuFilieresComponent;
  let fixture: ComponentFixture<MenuFilieresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuFilieresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuFilieresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
