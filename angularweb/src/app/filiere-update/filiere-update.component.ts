import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Filiere } from '../interfaces/filiere.interface';
import { FiliereService } from '../services/filiere.service';
import { FormationService } from '../services/formation.service';
import { Formation } from '../interfaces/formation.interface';
import { StagiaireService } from '../services/stagiaire.service';
import { Stagiaire } from '../interfaces/personne.interface';

@Component({
  selector: 'app-filiere-update',
  templateUrl: './filiere-update.component.html',
  styleUrls: ['./filiere-update.component.css']
})
export class FiliereUpdateComponent implements OnInit {

    filiere: Filiere = {
      libelle: '',
      stagiaires: [],
    }
  
    actionText= 'Modifier' ;
  constructor(private filiereService: FiliereService,
    private http:HttpClient,
     private router: Router, 
     private route:ActivatedRoute,
     private formationService:FormationService,
     private stagiaireService: StagiaireService) { }

  ngOnInit(): void {
    const id: string | null = this.route.snapshot.paramMap.get('id');
      this.filiereService.getById(Number(id))
        .subscribe((filiere: Filiere) => {
          this.filiere = filiere;
        })
    
  }
  handleSubmit() {
    if (this.filiere.id) {
      console.log('Updating filière...');
      const { id, libelle } = this.filiere;
      const filiere: Filiere = { id, libelle, stagiaires: [] };
      this.filiereService.update(filiere)
        .subscribe(() => {
          this.router.navigate(['/gestionnaire-filieres']);
        })
      }
    }
}
