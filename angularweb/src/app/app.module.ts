import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';



import { AppRoutingModule} from './app-routing.module';

import { AppComponent } from './app.component';
import { AccueilComponent } from './accueil/accueil.component';
import { SituationComponent } from './situation/situation.component';
import { StagiaireModuleComponent } from './stagiaire-module/stagiaire-module.component';
import { FormateurModuleComponent } from './formateur-module/formateur-module.component';
import { GestionnaireComponent } from './gestionnaire/gestionnaire.component';
import { FormateurFormationsComponent } from './formateur-formations/formateur-formations.component';
import { FormationsModifComponent } from './formations-modif/formations-modif.component';
import { GestionnaireModulesComponent } from './gestionnaire-modules/gestionnaire-modules.component';
import { GestionnaireFilieresComponent } from './gestionnaire-filieres/gestionnaire-filieres.component';
import { ModulesModifComponent } from './modules-modif/modules-modif.component';
import { FilieresModifComponent } from './filieres-modif/filieres-modif.component';
import { GestionnaireStagiairesComponent } from './gestionnaire-stagiaires/gestionnaire-stagiaires.component';
import { MenuCompteComponent } from './menu-compte/menu-compte.component';
import { MenuModulesComponent } from './menu-modules/menu-modules.component';
import { MenuFilieresComponent } from './menu-filieres/menu-filieres.component';
import { MenuGestionComponent } from './menu-gestion/menu-gestion.component';
import { DeconnexionComponent } from './deconnexion/deconnexion.component';
import { CommonModule } from '@angular/common';

import { InscriptionComponent } from './inscription/inscription.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { GestionnaireFormationsComponent } from './gestionnaire-formations/gestionnaire-formations.component';
import { GestionnaireFormateursComponent } from './gestionnaire-formateurs/gestionnaire-formateurs.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FiliereUpdateComponent } from './filiere-update/filiere-update.component';


@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    SituationComponent,
    StagiaireModuleComponent,
    FormateurModuleComponent,
    GestionnaireComponent,
    FormateurFormationsComponent,
    FormationsModifComponent,
    GestionnaireModulesComponent,
    GestionnaireFilieresComponent,
    ModulesModifComponent,
    FilieresModifComponent,
    GestionnaireStagiairesComponent,
    MenuCompteComponent,
    MenuModulesComponent,
    MenuFilieresComponent,
    MenuGestionComponent,
    DeconnexionComponent,
    HeaderComponent,
    FooterComponent,
    GestionnaireFormationsComponent,
    InscriptionComponent,
    HeaderComponent,
    FooterComponent,
    GestionnaireFormateursComponent,
    FiliereUpdateComponent
  ],

  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
