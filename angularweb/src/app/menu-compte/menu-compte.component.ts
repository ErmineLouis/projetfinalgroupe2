import { Component, OnInit } from '@angular/core';
import { Personne } from '../interfaces/personne.interface';
import { Utilisateur } from '../interfaces/utilisateur.interface';
import { NgForm } from '@angular/forms';
import { Filiere } from '../interfaces/filiere.interface';
import { FiliereService } from '../services/filiere.service';
import { Module } from '../interfaces/module.interface';
import { ModuleService } from '../services/module.service';
import { Formation } from '../interfaces/formation.interface';
import { FormationService } from '../services/formation.service';
@Component({
  selector: 'menu-compte',
  templateUrl: './menu-compte.component.html',
  styleUrls: ['./menu-compte.component.css']
})
export class MenuCompteComponent implements OnInit {

  personne: Personne = {
    id: 2,
    nom: "Ermine",
    prenom: "Louis",
    mail: "a@a",
    telephone: "090809",
    dateDeNaissance: new Date(2001, 4, 27),
    adresse: 'renens',
    type: "ADMIN",
    filiere_id: 1,
    formation_demandee: 'java'
  }

  utilisateur: Utilisateur = {
    id: 0,
    login: 'aze',
    dateInscription: new Date(),
    personne_id: this.personne.id,
    motDepasse: '123'
  }

  filiere: Filiere[] = [{
    libelle: 'az',
    id: 0,
    module: [],
    dateDebut: new Date(),
    dateFin: new Date()
  }]

  module:Module[]= [{
    id: 0,
    libelle: '',
    dateDebut: new Date(),
    dateFin: new Date(),
    formateur: undefined
  }]

  formation:Formation[] = [{

    libelle:"",
    filieres:[]
  }]



  constructor( private filiereService: FiliereService, private moduleService: ModuleService, private formationService : FormationService) {


  }

  async ngOnInit(): Promise<void> {
    if (this.personne.type == "STAGIAIRE") {
      if (this.personne.filiere_id !== null) {

        this.filiereService.getFiliere(this.personne.filiere_id).subscribe(value => this.filiere[0] = value)

      }
    } else if(this.personne.type == "FORMATEUR") {

      //formateur associé a filiere

      this.moduleService.getModuleFormateur(this.utilisateur.id).subscribe(value => this.module = value)


    } else {

      this.filiereService.getFilieres().subscribe(value => this.filiere = value)
      
      this.moduleService.getModules().subscribe(value => this.module = value)
      this.formationService.getFormations().subscribe(value => this.formation =value)

    }

  }

  handleSubmit() {

    console.log(this.filiere)

    console.log("update")


  }




}
