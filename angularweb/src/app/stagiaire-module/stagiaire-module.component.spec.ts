import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StagiaireModuleComponent } from './stagiaire-module.component';

describe('StagiaireModuleComponent', () => {
  let component: StagiaireModuleComponent;
  let fixture: ComponentFixture<StagiaireModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StagiaireModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StagiaireModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
