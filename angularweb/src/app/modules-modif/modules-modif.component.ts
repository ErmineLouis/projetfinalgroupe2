import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Filiere } from '../interfaces/filiere.interface';
import { Formateur } from '../interfaces/formateur.interface';
import { Module, ModulePost } from '../interfaces/module.interface';
import { FiliereService } from '../services/filiere.service';
import { FormateurService } from '../services/formateur.service';
import { ModuleService } from '../services/module.service';

const API='http://localhost:8082/api/personne/formateurs?filiere_id=2';
@Component({
  selector: 'modules-modif',
  templateUrl: './modules-modif.component.html',
  styleUrls: ['./modules-modif.component.css']
})
export class ModulesModifComponent implements OnInit {
 filieres:Filiere[]=[];
 formateurs: Formateur[]=[];
 formateurs2: Formateur[]=[]
 module: ModulePost = {
  libelle: "",
  dateDebut: "2022-01-01",
  dateFin: "2022-01-05",
  filiere: {id: 1},
  formateur: null
}
filiere_id:number=0;
actionText: 'Ajouter' | 'Modifier' = 'Ajouter';
  constructor(private moduleService: ModuleService,
     private router: Router,
    private route: ActivatedRoute,
     private filiereService:FiliereService,
     private formateurService:FormateurService) { }

  ngOnInit(): void {
    this.filiereService.getAll()
      .subscribe((filieres: Filiere[]) => {
        this.filieres = filieres;
        if (this.module.filiere && this.filieres[0].id) {
          this.module.filiere.id = this.filieres[0].id;
          
        }
        console.log(this.filiere_id)
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
          this.actionText = 'Modifier'
          this.moduleService.getById(Number(id))
            .subscribe((module: Module) => {
              this.module.id = module.id;
              this.module.libelle = module.libelle;
              this.module.dateDebut = module.dateDebut;
              this.module.dateFin = module.dateFin;
    
              if (module.filiere.id) {
                this.module.filiere.id = module.filiere.id;
                // console.log("ID afficher"+module.filiere.id);
                //  this.formateurService.getById(this.filiere_id).subscribe((formateurs:Formateur[])=>{console.log(formateurs)
                //  this.formateurs=formateurs})
                
              }
            })
        }
      })
  }
   handleSubmit(){if (this.module.id) {
    console.log('Updating module...');
    this.moduleService.update(this.module)
      .subscribe(() => {
        this.router.navigate(['/gestionnaire-modules']);
      })
  } else {
    console.log('Adding module...');
    this.moduleService.post(this.module)
      .subscribe(() => {
        this.router.navigate(['/gestionnaire-modules']);
      })
  }
}

handleCustomEvent(event: any){
  const option = event.target.value;
   this.filiere_id=option;
   console.log("filiere id "+this.filiere_id)
  this.formateurService.getById(this.filiere_id).subscribe((formateurs:Formateur[])=>{console.log(formateurs)
     this.formateurs=formateurs})
}


}
