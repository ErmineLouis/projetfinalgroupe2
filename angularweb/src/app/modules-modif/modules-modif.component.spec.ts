import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulesModifComponent } from './modules-modif.component';

describe('ModulesModifComponent', () => {
  let component: ModulesModifComponent;
  let fixture: ComponentFixture<ModulesModifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModulesModifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModulesModifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
