import { Component,OnInit } from '@angular/core';
import { DeconnexionService } from '../services/deconnexion.service';
import { Utilisateur } from '../interfaces/utilisateur.interface';
import { localStorage } from '../interfaces/localStorage.interface';
import { Personne } from '../interfaces/personne.interface';


@Component({
  selector: 'deconnexion',
  templateUrl: './deconnexion.component.html',
  styleUrls: ['./deconnexion.component.css']
})
export class DeconnexionComponent implements OnInit {

  constructor(private deconectionService:DeconnexionService) { }

  motDePasse:string ="";
  login:any="";
  personne_id:number =0;
  localstorage:localStorage =
  {
    type:"",
    nom:"",
    prenom:""
  }
   auth:any

  ngOnInit(): void {
  }
  findAccount()
  {
    this.deconectionService.getUtilisateur(this.login,this.motDePasse).subscribe((res:Utilisateur) =>{
      this.deconectionService.getPersonne(res.personne_id).subscribe((resu:Personne) =>
      {
        this.localstorage.type = resu.type
        this.localstorage.nom = resu.nom
        this.localstorage.prenom = resu.prenom
        this.connector();
        window.location.href="accueil"
      }
       );
    }
  );
  }
  connector()
  {
    localStorage.setItem('user', JSON.stringify(this.localstorage));
    this.isConnected();
  }
  public isConnected()
  {
    let json:any =localStorage.getItem("user");
    this.auth = JSON.parse(json);
  }


}
