import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionnaireStagiairesComponent } from './gestionnaire-stagiaires.component';

describe('GestionnaireStagiairesComponent', () => {
  let component: GestionnaireStagiairesComponent;
  let fixture: ComponentFixture<GestionnaireStagiairesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionnaireStagiairesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnaireStagiairesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
