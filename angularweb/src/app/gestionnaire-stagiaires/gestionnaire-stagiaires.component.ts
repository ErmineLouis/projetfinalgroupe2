import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Formation } from '../interfaces/formation.interface';
import { Stagiaire } from '../interfaces/stagiaire.interface';
import { FormationService } from '../services/formation.service';
import { StagiaireService } from '../services/stagiaire.service';
import { Filiere } from '../interfaces/filiere.interface';
import { FiliereService } from '../services/filiere.service';

@Component({
  selector: 'gestionnaire-stagiaires',
  templateUrl: './gestionnaire-stagiaires.component.html',
  styleUrls: ['./gestionnaire-stagiaires.component.css']
})
export class GestionnaireStagiairesComponent implements OnInit {

  filieres:Filiere[]=[];
  stagiare:Stagiaire={
    
    nom:"",
    prenom:""
  }
  filiere:Filiere={
    libelle:"",
    stagiaires :[]
  }
  stagiaires: Stagiaire[]=[];
  formations: Formation[]=[];
  constructor(private stService: StagiaireService, 
    private router: Router,
    private forservic:FormationService,
    private filiereService: FiliereService ) { }

  ngOnInit(): void {
    this.stService.getAll().subscribe((stagiaires:Stagiaire[])=>{this.stagiaires=stagiaires})
    this.forservic.getAll().subscribe((formations:Formation[]) =>{this.formations=formations})
    this.filiereService.getAll().
    subscribe((filieres:Filiere[])=>
    {this.filieres=filieres});
  }
 
  onEdit(){
    this.filiere.stagiaires.push();
  }
  handleCustomEvent(event:any){}

}
