import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionnaireFilieresComponent } from './gestionnaire-filieres.component';

describe('GestionnaireFilieresComponent', () => {
  let component: GestionnaireFilieresComponent;
  let fixture: ComponentFixture<GestionnaireFilieresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionnaireFilieresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnaireFilieresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
