import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Filiere } from '../interfaces/filiere.interface';
import { FiliereService } from '../services/filiere.service';

@Component({
  selector: 'gestionnaire-filieres',
  templateUrl: './gestionnaire-filieres.component.html',
  styleUrls: ['./gestionnaire-filieres.component.css']
})
export class GestionnaireFilieresComponent implements OnInit {
 filieres:Filiere[]=[];

  constructor(private filiereService: FiliereService, private http:HttpClient, private router: Router) {
    
    //test.subscribe(res=>console.log(res));
   const source$= filiereService.getAll();
   source$.subscribe((filieres:Filiere[])=>{this.filieres=filieres})
   }

  ngOnInit(): void {
    this.filiereService.getAll().
    subscribe((filieres:Filiere[])=>
    {this.filieres=filieres});
    //console.log("test")
  }
  onDelete(id?:number) {
    if (id) {
      this.filiereService.delete(id).subscribe(() => {
          this.filieres = this.filieres.filter(filiere => filiere.id !== id)
        })
    }
  }
  onEdit(id?:number){
  this.router.navigate(['/filiere/update', id])
  }
}
