

import { Component,OnInit } from '@angular/core';
import { localStorage } from '../interfaces/localStorage.interface';
import { DeconnexionService } from '../services/deconnexion.service';


@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  user: localStorage = {
    type:"",
    nom:"",
    prenom:"",

  };
  auth:any = null;
  constructor()
  {
    this.isConnected();
  }

  ngOnInit(): void {
    this.isConnected();
  }
   public isConnected()
  {
    let json:any =localStorage.getItem("user");
    this.auth = JSON.parse(json);
  }

  handleDisconnect()
  {
   this.auth = null
   localStorage.clear();
  }



}
