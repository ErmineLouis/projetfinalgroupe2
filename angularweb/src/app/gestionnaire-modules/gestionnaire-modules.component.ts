import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModuleService } from '../services/module.service';
import { Module } from '../interfaces/module.interface';
@Component({
  selector: 'gestionnaire-modules',
  templateUrl: './gestionnaire-modules.component.html',
  styleUrls: ['./gestionnaire-modules.component.css']
})
export class GestionnaireModulesComponent implements OnInit {
  modules: Module[]=[]
  constructor(private moduleService: ModuleService, 
    private router: Router) { 
      this.moduleService.getAll()
      .subscribe((modules: Module[]) => {
        this.modules = modules;
      })
    }

  ngOnInit(): void {
  }
  onDelete(id?: number){
    if (id) {
      this.moduleService.delete(id)
      .subscribe(res => {
        console.log(res);
        this.modules = this.modules.filter(module => module.id !== id);
      })
    }
    this.router.navigate(['/gestionnaire-modules'])
  }
  onEdit(id?: number){
    this.router.navigate(['/modules/modif', id])}
}
