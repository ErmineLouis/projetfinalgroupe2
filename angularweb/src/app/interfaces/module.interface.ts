
import { Filiere } from "./filiere.interface";
import { Personne } from "./personne.interface";
export interface Module {

    id:number;

    libelle:string;

    dateDebut:Date;

    dateFin:Date;
    formateur?:Personne;

    filiere:Filiere
}
type EntityId = {
  id: number;
}

export interface ModulePost {
  id?: number;
  libelle: string;
  dateDebut: string; 
  dateFin: string;
  formateur: EntityId | null;
  filiere: EntityId;
}

