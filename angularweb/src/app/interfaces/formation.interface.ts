

import { Filiere } from "./filiere.interface"


export interface Formation {
    id?:string;

    libelle:string;

    filieres?: Filiere[]
}

