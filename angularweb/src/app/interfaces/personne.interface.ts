
export interface Personne{
  id?: number;
  nom: string;
  prenom: string;
  type: string;
  filiere_id?: number;
  adresse: string;
  dateDeNaissance: Date;
  formation_demandee: string;
  mail: string;
  telephone: string;
}

