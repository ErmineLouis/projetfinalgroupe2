import { Formation } from "./formation.interface";

export interface Stagiaire{
    id?:number;
    nom:string;
    prenom:string;
    mail?:string;
    tetephone?:string;
    dateInscription?:string;
    dateDeNaissance?:string;
    adresse?:string;
    formation?: Formation;
}