import { Module } from "./module.interface";


export interface Filiere {

    libelle:string;
    id:number;
    module:Module[];
    dateDebut:Date;
    dateFin:Date;
    stagiaires: [];



}

