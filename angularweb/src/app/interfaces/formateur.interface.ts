import { Formation } from "./formation.interface";
import { Module } from "./module.interface";

export interface Formateur{
    id:number;
    nom:string;
    prenom:string;
    mail?:string;
    tetephone?:string;
    dateDeNaissance?:string;
    adresse?:string;
    modules?: Module[];
}