
export interface Utilisateur{
    id?: number;
    motDepasse: String;
    dateInscription?: Date;
    login: String;
    personne_id?: number;
}
