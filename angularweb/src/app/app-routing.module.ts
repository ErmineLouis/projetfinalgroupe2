import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { AccueilComponent } from './accueil/accueil.component';
import { DeconnexionComponent } from './deconnexion/deconnexion.component';
import { FilieresModifComponent } from './filieres-modif/filieres-modif.component';
import { FormateurFormationsComponent } from './formateur-formations/formateur-formations.component';
import { FormateurModuleComponent } from './formateur-module/formateur-module.component';
import { FormationsModifComponent } from './formations-modif/formations-modif.component';
import { GestionnaireComponent } from './gestionnaire/gestionnaire.component';

import { InscriptionComponent } from './inscription/inscription.component';
import { RouterModule, Routes } from '@angular/router';
import { MenuFilieresComponent } from './menu-filieres/menu-filieres.component';

import { SituationComponent } from './situation/situation.component';
import { MenuModulesComponent } from './menu-modules/menu-modules.component';
import { FiliereService } from './services/filiere.service';
import { ModulesModifComponent } from './modules-modif/modules-modif.component';
import { MenuCompteComponent } from './menu-compte/menu-compte.component';
import { GestionnaireFilieresComponent } from './gestionnaire-filieres/gestionnaire-filieres.component';
import { GestionnaireModulesComponent } from './gestionnaire-modules/gestionnaire-modules.component';
import { GestionnaireStagiairesComponent } from './gestionnaire-stagiaires/gestionnaire-stagiaires.component';
import { GestionnaireFormationsComponent } from './gestionnaire-formations/gestionnaire-formations.component';
import { GestionnaireFormateursComponent } from './gestionnaire-formateurs/gestionnaire-formateurs.component';
import { FiliereUpdateComponent } from './filiere-update/filiere-update.component';



const routes: Routes = [

{path: 'accueil', component : AccueilComponent},
{path: 'inscription', component : InscriptionComponent},
{path: 'authentification', component : DeconnexionComponent},
{path: 'formateur/formations', component : FormateurFormationsComponent},
{path: 'stagiaire/formations', component : MenuFilieresComponent},
{path: 'stagiaire/modules', component : MenuModulesComponent},
{path: 'formateur/modules', component : FormateurModuleComponent},
{path: 'filiere/gestion', component : FilieresModifComponent},
{path: 'formations/gestion', component : FormationsModifComponent},
{path: 'modules/gestion', component : ModulesModifComponent},
{path: 'tableaudebord', component : GestionnaireComponent}, // gestionnaire
{path: 'moncompte', component : MenuCompteComponent}, // menu-compte
{path: 'accueil', component : AccueilComponent},
{path: 'authentification', component : DeconnexionComponent},
{path: 'filieres/modif', component : FilieresModifComponent},
{path: 'formateur/formations', component : FormateurFormationsComponent},
{path: 'formateur/module', component : FormateurModuleComponent},
{path: 'formations/modif', component : FormationsModifComponent},
{path: 'formations/modif/:id', component : FormationsModifComponent},
{path: 'gestionnaire', component : GestionnaireComponent}, 
{path: 'modules/gestion', component : ModulesModifComponent}, 
{path: 'tableaubord', component : GestionnaireComponent},
{path: 'moncompte',component : MenuCompteComponent},
{path: 'gestionnaire-filieres',component : GestionnaireFilieresComponent},
{path: 'gestionnaire-modules', component :GestionnaireModulesComponent},
{path: 'gestionnaire-stagiaires', component : GestionnaireStagiairesComponent},
{path: 'gestionnaire-formations', component : GestionnaireFormationsComponent}, 
{path: 'gestionnaire-formations', component : GestionnaireFormationsComponent},
{path: 'gestionnaire-formateurs', component : GestionnaireFormateursComponent}, 
{path: 'modules/modif', component: ModulesModifComponent},
{path: 'modules/modif/:id', component: ModulesModifComponent},
{path: 'filiere/update/:id', component: FiliereUpdateComponent}

];
@NgModule({


  imports: [HttpClientModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule { }

