import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Formation } from '../interfaces/formation.interface';
import { Personne } from '../interfaces/personne.interface';
import { Utilisateur } from '../interfaces/utilisateur.interface';



const API = "http://localhost:8082/api/utilisateur"
const APIFormation = "http://localhost:8082/api/formation";
const APIUtilisateur = "http://localhost:8082/api/utilisateur";
const APIMail = "http://localhost:8082/api/personne/mail";

@Injectable({
  providedIn: 'root'
})

export class UtilisateurService {
  constructor(private http: HttpClient) { }

  postPersonne(personne: Personne): Observable<HttpResponse<Personne>> {
    const headers: HttpHeaders = new HttpHeaders();
    return this.http.post<Personne>(API, personne, { headers, observe: 'response' });
  }

  getPersonne(personne: Personne): Observable<Personne> {
    return this.http.get<Personne>(APIMail + '/' + personne.mail);
  }

  postUtilisateur(utilisateur: Utilisateur): Observable<HttpResponse<Utilisateur>> {
    const headers: HttpHeaders = new HttpHeaders();
    return this.http.post<Utilisateur>(APIUtilisateur, utilisateur, { headers, observe: 'response' });
  }

  getFormations(): Observable<Formation[]> {
    return this.http.get<Formation[]>(APIFormation);
  }

}
