import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Stagiaire } from '../interfaces/personne.interface';

const API2='http://localhost:8082/api/personne/formation?formation_choose='
const API='http://localhost:8082/api/personne/candidat'
@Injectable({
  providedIn: 'root'
})
export class StagiaireService {

  constructor(private http:HttpClient) { }

  getByFomationLibelle(id: string): Observable<Stagiaire[]> {
    return this.http.get<Stagiaire[]>(API2 + id);
  }
  getAll(): Observable<Stagiaire[]>{
    return this.http.get<Stagiaire[]>(API);
  }

}
