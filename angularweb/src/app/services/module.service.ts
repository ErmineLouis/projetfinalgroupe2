import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Module, ModulePost } from '../interfaces/module.interface';


const API = "http://localhost:8082/api/module"

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  constructor(private httpClient : HttpClient) { }


  getModules():Observable<Module[]> {

    return this.httpClient.get<Module[]>(API)

  }

  getModuleFormateur(id? : number):Observable<Module[]> {

    return this.httpClient.get<Module[]>(API+"/formation/" + id)

  }

    getAll(): Observable<Module[]> {
      return this.httpClient.get<Module[]>(API);
    }
  
    getById(id: number): Observable<Module> {
      return this.httpClient.get<Module>(API + '/' + id)
    }
  
    post(module: ModulePost) {
      return this.httpClient.post(API, module);
    }
  
    update(module: ModulePost) {
      return this.httpClient.put(API, module);
    }
  
    delete(id: number) {
      return this.httpClient.delete(API + '/' + id);
    }

  
}
