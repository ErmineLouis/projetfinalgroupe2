import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Utilisateur } from '../interfaces/utilisateur.interface';
import { Personne } from '../interfaces/personne.interface';

@Injectable({
  providedIn: 'root'
})
export class DeconnexionService {
// lien API pour les utilisateurs
private API:string = 'http://localhost:8082/api/utilisateur';
// lien API pour les personne
private API2:string ='http://localhost:8082/api/personne';

constructor(private http: HttpClient) { }

  //methode afficher tout
  getUtilisateur(login:string,mdp:string):Observable<Utilisateur>
{
  return this.http.get<Utilisateur>(this.API+'/'+login+'/'+mdp );
}
 getPersonne(personne_id:number):Observable<Personne>
 {
    return this.http.get<Personne>(this.API2+'/'+personne_id);
 }
}
