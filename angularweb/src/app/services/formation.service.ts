import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Formation } from '../interfaces/formation.interface';


const API ="http://localhost:8082/api/formation"


@Injectable({
  providedIn: 'root'
})
export class FormationService {


  constructor(private httpClient : HttpClient) { }

  getFormations():Observable<Formation[]> {

    return this.httpClient.get<Formation[]>(API)

  }

  getAll(): Observable<Formation[]>{
    return this.httpClient.get<Formation[]>(API);
  }
  
  getById(id: string): Observable<Formation> {
    return this.httpClient.get<Formation>(API + '/' + id);
  }
  
  post(filiere: Formation) {
    return this.httpClient.post(API, filiere);
  }
  
  update(filiere: Formation) {
    return this.httpClient.put(API, filiere);
  }
  
  delete(id: string) {
    return this.httpClient.delete(API + '/' + id);
  }

}




