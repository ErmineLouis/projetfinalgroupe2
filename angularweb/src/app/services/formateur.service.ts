import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Formateur } from '../interfaces/formateur.interface';

const API='http://localhost:8082/api/personne/formateurs?filiere_id=1';
const API2='http://localhost:8082/api/personne/formateurs?filiere_id=';



@Injectable({
  providedIn: 'root'
})
export class FormateurService {

  constructor(private http: HttpClient) { }


  getAll(): Observable<Formateur[]>{
    return this.http.get<Formateur[]>(API );
  }
  
  getById(id: number): Observable<Formateur[]> {
    return this.http.get<Formateur[]>(API2 + id);
  }
  
  // post(filiere: Formateur) {
  //   return this.http.post(API, filiere);
  // }
  
  // update(filiere: Formateur) {
  //   return this.http.put(API, filiere);
  // }
  
  delete(id: number) {
    return this.http.delete(API + '/' + id);
  }






}
