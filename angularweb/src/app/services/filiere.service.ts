
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Filiere } from '../interfaces/filiere.interface';
import { getLocaleCurrencyCode } from '@angular/common';



const API ="http://localhost:8082/api/filiere"

@Injectable({
  providedIn: 'root'
})
export class FiliereService {

  constructor(private httpClient : HttpClient) { }


  getFiliere(id?:number):Observable<Filiere>{

    return this.httpClient.get<Filiere>(API +"/detail/"+id)
  }

  getFilieres():Observable<Filiere[]> {

    return this.httpClient.get<Filiere[]>(API)
  }

  getAll(): Observable<Filiere[]>{
    return this.httpClient.get<Filiere[]>(API);
  }

  getById(id: number): Observable<Filiere> {
    return this.httpClient.get<Filiere>(API + '/' + id);
  }

  post(filiere: Filiere) {
    return this.httpClient.post(API, filiere);
  }

  update(filiere: Filiere) {
    return this.httpClient.put(API, filiere);
  }

  delete(id: number) {
    return this.httpClient.delete(API + '/' + id);
  }



}



// getAll(): Observable<Filiere[]> {
//   return this.http.get<Filiere[]>(API);
// }