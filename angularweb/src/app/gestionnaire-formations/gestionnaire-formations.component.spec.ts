import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionnaireFormationsComponent } from './gestionnaire-formations.component';

describe('GestionnaireFormationsComponent', () => {
  let component: GestionnaireFormationsComponent;
  let fixture: ComponentFixture<GestionnaireFormationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionnaireFormationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnaireFormationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
