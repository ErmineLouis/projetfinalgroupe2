import { Component, OnInit } from '@angular/core';



import { Formation } from '../interfaces/formation.interface';
import { FormationService } from '../services/formation.service';
import { filter } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'gestionnaire-formations',
  templateUrl: './gestionnaire-formations.component.html',
  styleUrls: ['./gestionnaire-formations.component.css']
})
export class GestionnaireFormationsComponent implements OnInit {


  formations:Formation[] =[]
  formation: Formation={
  libelle:""
  }
  constructor(private formationService:FormationService, private router: Router) {

   }

  ngOnInit(): void {
    this.formationService.getAll()
    .subscribe((formations:Formation[]) =>{
      this.formations = formations})
  }

  onDelete(id?:string){
    if (id) {
    this.formationService.delete(id)
    .subscribe(()=>{this.formations = this.formations.filter(formation => formation.id!== id)
    })
  }

  }
  onEdit(id?:string){
    this.router.navigate(['/formations/modif', id])

  }

}
