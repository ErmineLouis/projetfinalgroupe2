

import { Component, OnInit } from '@angular/core';
import { Personne } from '../interfaces/personne.interface';
import { Utilisateur } from '../interfaces/utilisateur.interface';

import { Formation } from '../interfaces/formation.interface';
import { HttpResponse } from '@angular/common/http';
import { UtilisateurService } from '../services/utilisateur.service';

@Component({
  selector: 'inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {



  formations: Formation[]= [];

  formationChoisie: Formation = {
    id: "0",
    libelle: ""
  }

  personne: Personne = {
    nom: "",
    prenom: "",
    mail: "",
    telephone: "",
    dateDeNaissance: new Date(),
    adresse: "" ,
    formation_demandee: "",
    type: "NOUVEAU"
  };

  utilisateur: Utilisateur = {
    motDepasse: '',
    login: '',
    personne_id: 0
  };

  id: number = 0;

  clic: Boolean = false;

  constructor(private utilisateurService: UtilisateurService) {}

  handleCustomEvent(event: any){
     const option = event.target.value;
     this.formationChoisie.libelle = option;
  }

  handleSubmit(){
    this.utilisateurService.postPersonne(this.personne).subscribe((res: HttpResponse<Personne>) => {
    
      setTimeout( () => {
        if(this.personne.nom != ""){
          this.utilisateurService.getPersonne(this.personne).subscribe((resultat: Personne) => {
           
            this.personne = resultat;
            this.id = Number(resultat.id);
            this.utilisateur.motDepasse = this.personne.nom + "_" + this.personne.prenom;
            this.utilisateur.dateInscription =  new Date(Date.now());
            this.utilisateur.login = this.personne.prenom.charAt(0) + this.personne.nom;
            this.utilisateur.personne_id = Number(this.personne.id);
      
            this.utilisateurService.postUtilisateur(this.utilisateur).subscribe((res: HttpResponse<Utilisateur>) => {
              this.clic = true;
            });
          });
        }
      }, 5000);
    });  
  }

  ngOnInit(): void {
    this.utilisateurService.getFormations().subscribe((res: any) => this.formations = res);
  }


}
