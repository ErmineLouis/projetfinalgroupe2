import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionnaireFormateursComponent } from './gestionnaire-formateurs.component';

describe('GestionnaireFormateursComponent', () => {
  let component: GestionnaireFormateursComponent;
  let fixture: ComponentFixture<GestionnaireFormateursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionnaireFormateursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnaireFormateursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
