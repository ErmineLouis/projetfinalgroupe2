import { Component, OnInit } from '@angular/core';

import { Formateur } from '../interfaces/formateur.interface';
import { FormateurService } from '../services/formateur.service';


@Component({
  selector: 'gestionnaire-formateurs',
  templateUrl: './gestionnaire-formateurs.component.html',
  styleUrls: ['./gestionnaire-formateurs.component.css']
})
export class GestionnaireFormateursComponent implements OnInit {


  formateurs:Formateur[] =[]

  constructor(private formateurService:FormateurService) {
    const source$= formateurService.getAll()
    source$.subscribe((formateurs:Formateur[]) =>{this.formateurs= formateurs})
   }


  ngOnInit(): void {
  }


  onDelete(){

  }
  onEdit(){}


}
