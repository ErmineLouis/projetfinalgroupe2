package fr.formation.correction.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Formation {

    @Id
    private String libelle;

    @ManyToMany(mappedBy = "formations")
    private List<Filiere> filieres;

    public Formation () {

    }

    public Formation(String libelle, List<Filiere> filieres) {
        this.libelle = libelle;
        this.filieres = filieres;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public List<Filiere> getFilieres() {
        return filieres;
    }

    public void setFilieres(List<Filiere> filieres) {
        this.filieres = filieres;
    }

    @Override
    public String toString() {
        return "Formation [filieres=" + filieres + ", libelle=" + libelle + "]";
    }

    

    

    


    
    
}
