package fr.formation.correction.model;

import java.time.LocalDate;


import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@SequenceGenerator (name = "utilisateur_gen", sequenceName = "utilisateur_seq", initialValue = 100, allocationSize = 1)
public class Utilisateur {

    @Id
	@GeneratedValue(generator = "utilisateur_gen")
    private Integer id;

    private String login;

    private String motDePasse;

    private LocalDate dateInscription;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "personne_id", referencedColumnName = "id")

    @JsonIgnoreProperties("utilisateur")
    private Personne personne;

    public Utilisateur(){}
    ;

    public Utilisateur(Integer id, String login, String motDepasse, LocalDate dateInscription, Personne personne) {
        this.id = id;
        this.login = login;
        this.motDePasse = motDepasse;
        this.dateInscription = dateInscription;
        this.personne = personne;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getMotDepasse() {
        return motDePasse;
    }
    public void setMotDepasse(String motDepasse) {
        this.motDePasse = motDepasse;
    }
    public LocalDate getDateInscription() {
        return dateInscription;
    }
    public void setDateInscription(LocalDate dateInscription) {
        this.dateInscription = dateInscription;
    }
    public Personne getPersonne() {
        return personne;
    }
    public void setPersonne(Personne personne) {
        this.personne = personne;
    }
    @Override
    public String toString() {
        return "Utilisateur [dateInscription=" + dateInscription + ", id=" + id + ", login=" + login + ", motDePasse="
                + motDePasse + ", personne=" + personne + "]";
    }

    

    
    
}
