package fr.formation.correction.model;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@SequenceGenerator(name = "filiere_gen", sequenceName = "filiere_seq", initialValue = 100, allocationSize = 1)
public class Filiere {

	@Id
	@GeneratedValue(generator = "filiere_gen")
	private Integer id;

	private String libelle;

	@OneToMany(mappedBy = "filiere")
	@JsonIgnoreProperties("filiere")
	//@JsonIgnore
	private List<Module> modules;

	@JsonIgnoreProperties("filiere")
	@OneToMany(mappedBy = "filiere")
	//@JsonIgnore
	private List<Personne> stagiaires;

	@ManyToMany
	@JoinTable(name = "filiere_formation", 
	joinColumns = @JoinColumn(name = "filiere_id"), 
	inverseJoinColumns = @JoinColumn(name = "formation_id"))

	@JsonIgnoreProperties("formation")

	private List<Formation> formations;

	public Filiere() {

	}

	public Filiere(Integer id, String libelle, List<Module> modules, List<Personne> stagiaire, List<Formation> formations) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.modules = modules;
		this.stagiaires = stagiaire;
		this.formations = formations;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<Personne> getStagiaires() {
		return stagiaires;
	}

	public void setStagiaires(List<Personne> stagiaire) {
		this.stagiaires = stagiaire;
	}

	public List<Formation> getFormations() {
		return formations;
	}

	public void setFormations(List<Formation> formations) {
		this.formations = formations;
	}

	

}
