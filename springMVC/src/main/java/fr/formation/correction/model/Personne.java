package fr.formation.correction.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@SequenceGenerator(name = "personne_gen", sequenceName = "personne_seq", initialValue = 100, allocationSize = 1)
public class Personne {

	@Id
	@GeneratedValue(generator = "personne_gen")
	private Integer id;

	private String nom;

	private String prenom;

	private String mail;

	private String telephone;

	private LocalDate dateDeNaissance;

	private String adresse;

	private String formation_demandee;

	@OneToOne(mappedBy = "personne")

    @JsonIgnoreProperties("personne")

    private Utilisateur utilisateur;

	@Enumerated(EnumType.STRING)
	private TypePersonne type;

	@ManyToOne
	@JoinColumn(name = "filiere_id")
	@JsonIgnoreProperties({"stagiaires", "module"})
	//@JsonIgnore
	private Filiere filiere;

	@OneToMany(mappedBy = "formateur")
	@JsonIgnoreProperties({"formateur", "filiere"})
	//@JsonIgnore
	private List<Module> modules;

	public Personne() {

	}


	public Personne(Integer id, String nom, String prenom, Utilisateur utilisateur, TypePersonne type, Filiere filiere, List<Module> modules, LocalDate dateNaissance) {


		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.type = type;
		this.utilisateur = utilisateur;
		this.filiere = filiere;
		this.modules = modules;

		this.dateDeNaissance = dateNaissance;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public TypePersonne getType() {
		return type;
	}

	public void setType(TypePersonne type) {
		this.type = type;
	}

	

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}


	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public LocalDate getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(LocalDate dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getFormation_demandee() {
		return formation_demandee;
	}

	public void setFormation_demandee(String formation_demandee) {
		this.formation_demandee = formation_demandee;
	}

	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", type=" + type + "]";
	}
}
