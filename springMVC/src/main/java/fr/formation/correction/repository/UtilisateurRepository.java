package fr.formation.correction.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.correction.model.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Integer> {

    Utilisateur findByLogin(String login);



}