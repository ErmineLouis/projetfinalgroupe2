package fr.formation.correction.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.correction.model.Filiere;

public interface FiliereRepository extends JpaRepository<Filiere, Integer> {

}
