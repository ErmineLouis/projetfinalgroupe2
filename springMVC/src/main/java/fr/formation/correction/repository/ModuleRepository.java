package fr.formation.correction.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Module;

public interface ModuleRepository extends JpaRepository<Module, Integer> {

	List<Module> findByFiliere(Filiere f);

	List<Module> findByFormateurIsNull();
	List<Module> findByFormateur(String id);
}
