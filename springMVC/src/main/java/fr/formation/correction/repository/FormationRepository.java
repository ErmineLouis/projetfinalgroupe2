package fr.formation.correction.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Formation;

public interface FormationRepository extends JpaRepository<Formation, String>{

    List<Filiere> findFiliereByLibelle(String libelle);


    
}
