package fr.formation.correction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.correction.model.Utilisateur;
import fr.formation.correction.service.UtilisateurService;


@RestController
@RequestMapping("/api/utilisateur")
@CrossOrigin("*")
public class UtilisateurController {

    @Autowired
    private UtilisateurService us;
	

    @GetMapping("")
    public List<Utilisateur> findAll() {

        System.out.println(us.findAll() + "jsjdfh");
        
        return us.findAll();
    }

    @GetMapping("/{id}")
	public Utilisateur getById(@PathVariable Integer id) {
		return us.getById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "L'utilisateur n'existe pas"));
	}

    @PostMapping("")
	public void createUtilisateur(@RequestBody Utilisateur p) {
		us.create(p);
	}

    @PutMapping("")
	public void updateUtilisateur(@RequestBody Utilisateur p) {
		us.update(p);
	}

	@PutMapping("/{id}")
	public void deleteUtilisateur(@PathVariable Integer id) {
		us.delete(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "L'utilisateur n'existe pas"));
	}

	


    
}
