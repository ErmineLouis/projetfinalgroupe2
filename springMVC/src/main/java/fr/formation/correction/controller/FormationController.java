
package fr.formation.correction.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.DeleteMapping;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Formation;

import fr.formation.correction.service.FormationService;


@CrossOrigin("*")

@RestController
@RequestMapping("/api/formation")
public class FormationController {

    @Autowired
    private FormationService fs;

    @GetMapping("")
    public List<Formation> findAll() {
        
        return fs.findAll();
    }

    @GetMapping("/{id}")
	public Formation getById(@PathVariable String id) {
		return fs.getById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La formation n'existe pas"));
	}

    @PostMapping("")
	public void createUtilisateur(@RequestBody Formation p) {
		fs.create(p);
	}

    @PutMapping("")
	public void updateUtilisateur(@RequestBody Formation p) {
		fs.update(p);
	}
    @DeleteMapping("/{id}")

	public void deleteUtilisateur(@PathVariable String id) {
		fs.delete(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La formation n'existe pas"));
	}

    @GetMapping("/filiereFormation/{id}")
	public List<Filiere> getFilieres(@PathVariable String id) {
		return fs.findFiliereById(id);
	}



}

