package fr.formation.correction.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import fr.formation.correction.model.*;

import fr.formation.correction.repository.FormationRepository;

@Service
public class FormationService {

    @Autowired
    private FormationRepository fr;

    public void create(Formation m) {
		this.fr.save(m);
	}
	
	public List<Formation> findAll() {
		return this.fr.findAll();
	}
	
	public Optional<Formation> getById(String id) {
		return this.fr.findById(id);
    }
	
	public void update(Formation m) {
		this.fr.save(m);
	}
	
	public Optional<Boolean> delete(Formation m) {
		try {
			this.fr.delete(m);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public Optional<Boolean> delete(String id) {
		try {
			this.fr.deleteById(id);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public List<Filiere> findFiliereById(String id) {
		
		return fr.findFiliereByLibelle(id);
	}




    
}
