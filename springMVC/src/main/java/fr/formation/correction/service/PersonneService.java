package fr.formation.correction.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Formation;
import fr.formation.correction.model.Personne;
import fr.formation.correction.model.TypePersonne;
import fr.formation.correction.repository.PersonneRepository;

@Service
public class PersonneService {

	@Autowired
	private PersonneRepository pr;

	public void create(Personne p) {
		this.pr.save(p);
	}

	public List<Personne> findAll() {
		return this.pr.findAll();
	}

	public Optional<Personne> getById(Integer id) {
		return pr.findById(id);
	}

	public void update(Personne p) {
		this.pr.save(p);
	}

	public Optional<Boolean> delete(Personne p) {
		try {
			this.pr.delete(p);
			return Optional.of(Boolean.TRUE);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public Optional<Boolean> delete(Integer id) {
		try {
			this.pr.deleteById(id);
			return Optional.of(Boolean.TRUE);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public List<Personne> getFormateursOfFiliere(Filiere f) {
		return pr.findFormateursOfFiliere(f);
	}
	public List<Personne> getstagiareWTHFormation() {
		return pr.findStagiaireWTHFormation();
	}
	public List<Personne> getStagiaireOfFormation(String f) {
		return pr.findStagiaireOfFormation(f);
	}
	/*
	 * /!\ Cette méthode ne lancera pas d'exception si la personne n'est pas un
	 * stagiaire Attention à gérer ce comportement avant d'appeler cette méthode
	 */
	public void bindStagiairesToFiliere(List<Personne> pList, Filiere f) {
		for (Personne p : pList) {
			pr.bindPersonneToFiliere(p.getId(), f);
		}
	}

	/*
	 * /!\ Cette méthode ne lancera pas d'exception si la personne n'est pas un
	 * stagiaire Attention à gérer ce comportement avant d'appeler cette méthode
	 */
	public void bindStagiaireToFiliere(Personne p, Filiere f) {
		pr.bindPersonneToFiliere(p.getId(), f);
	}

	public Boolean checkAllStagiaire(List<Personne> pList) {
		for (Personne p : pList) {
			if (!TypePersonne.STAGIAIRE.equals(pr.getById(p.getId()).getType()))
				return false;
		}
		return true;
	}
}
