package fr.formation.correction.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.correction.model.Personne;
import fr.formation.correction.model.Utilisateur;
import fr.formation.correction.repository.UtilisateurRepository;

@Service
public class UtilisateurService {

    @Autowired
    private UtilisateurRepository us;



    public void create(Utilisateur u) {

        this.us.save(u);

	}
	
    public List<Utilisateur> findAll() {
		return this.us.findAll();
	}
	
	public Optional<Utilisateur> getById(Integer id) {
		return this.us.findById(id);
	}
	
	public void update(Utilisateur m) {
		this.us.save(m);
	}
	
	public Optional<Boolean> delete(Utilisateur u) {
		try {
			this.us.delete(u);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public Optional<Boolean> delete(Integer id) {
		try {
			this.us.deleteById(id);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public Utilisateur findByLogin(String login) {

        return this.us.findByLogin(login);
    }

    public Personne findPersonne(String login) {
        
        return this.us.findByLogin(login).getPersonne();
    }

	
    
}
